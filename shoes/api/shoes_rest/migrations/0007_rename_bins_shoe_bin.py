# Generated by Django 4.0.3 on 2022-06-16 23:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0006_alter_binvo_options'),
    ]

    operations = [
        migrations.RenameField(
            model_name='shoe',
            old_name='bins',
            new_name='bin',
        ),
    ]
