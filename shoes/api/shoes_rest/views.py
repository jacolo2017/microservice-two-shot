from django.http import JsonResponse
from django.shortcuts import render
from common.json import ModelEncoder
from .models import BinVO, Shoe
from django.views.decorators.http import require_http_methods
import json

# Create your views here.
class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href", "bin_number"]




class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "name",
        "color",
        "picture_url",
    ]


    def get_extra_data(self, o):
        return {"bin": o.bin.bin_number}
    # encoders = {
    #     'bin': BinVOEncoder()
    #     }


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "name",
    ]
    def get_extra_data(self, o):
        return {"bin": o.bin.bin_number}


@require_http_methods(["GET", "POST"])
def api_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {'shoes': shoes},
            encoder=ShoeListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            # href = content["bin"]
            # bins = BinVO.objects.get(import_href=href)
            # content["bin"] = bins
            bins = BinVO.objects.get(import_href=content["bin"])
            content["bin"] = bins
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
        shoe,
        encoder=ShoeDetailEncoder,
        safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_shoe(request, pk):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.all()
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            count, _ = Shoe.objects.filter(id=pk).delete()
            return JsonResponse({'deleted': count > 0})
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Shoe does not exist"})
    else:
        try:
            content = json.loads(request.body)
            shoe = Shoe.objects.get(id=pk)

            props = ['manufacturer', 'name', 'color', 'picture_url', 'bins']
            for prop in props:
                if prop in content:
                    setattr(shoe, prop, content[prop])
            shoe.save()
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Shoe does not exist"})
            response.status_code = 404
            return response
