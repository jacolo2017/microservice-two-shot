from django.db import models
from django.urls import reverse

# from wardrobe.api.wardrobe_api.models import Bin, 
class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=True)
    closet_name = models.CharField(max_length=100, null=True)
    bin_number = models.PositiveSmallIntegerField(null=True, blank=True)
    bin_size = models.PositiveSmallIntegerField(null=True, blank=True)

    # def get_api_url(self):
    #     return reverse("api_bin", kwargs={"pk": self.pk})

    # def __str__(self):
    #     return f"{self.closet_name} - {self.bin_number}/{self.bin_size}"

    # class Meta:
    #     ordering = ("closet_name", "bin_number", "bin_size")

# Create your models here.
class Shoe(models.Model): 
    manufacturer = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(max_length=300, null=True, blank=True)
    bins = models.ForeignKey(
        BinVO,
        related_name='+',
        on_delete=models.PROTECT,
        null=True,
    )
