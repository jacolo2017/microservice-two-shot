import React from 'react';

class ShoeForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            manufacturer: '',
            name: '',
            color: '',
            picture_url: '',
            bin: '',
            bins: []
        }
        this.handleChangeName = this.handleChangeName.bind(this)
        this.handleChangeManufacturer = this.handleChangeManufacturer.bind(this)
        this.handleChangeColor = this.handleChangeColor.bind(this)
        this.handleChangePictureUrl = this.handleChangePictureUrl.bind(this)
        this.handleChangeBin = this.handleChangeBin.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/bins'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            console.log(data)
            this.setState({ bins: data.bins })
        }
    }

    handleChangeName(event) {
        const value = event.target.value
        this.setState({name: value})
    }

    handleChangeManufacturer(event) {
        const value = event.target.value
        this.setState({manufacturer: value})
    }

    handleChangeColor(event) {
        const value = event.target.value
        this.setState({color: value})
    }

    handleChangePictureUrl(event) {
        const value = event.target.value
        this.setState({picture_url: value})
    }

    handleChangeBin(event) {
        const value = event.target.value
        this.setState({bin: value})
    }

    async handleSubmit(event) {
        event.preventDefault()
        const data = {... this.state}
        delete data.bins
        console.log(data)

        const shoeUrl = `http://localhost:8080/api/shoes/`
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        }
        const response = await fetch(shoeUrl, fetchConfig)
        if (response.ok) {
            const newShoe = await response.json()
            console.log(newShoe)
            this.setState({
                manufacturer: '',
                name: '',
                color: '',
                pictureUrl: '',
                bin: '',
            })
        }
    }

    render() {
        return (
            <div className="px-4 py-5 my-5 text-center">
                <h1 className="display-5 fw-bold">Add Your Shoes</h1>
                <form onSubmit={this.handleSubmit} id='create-shoe-form'>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleChangeManufacturer} value={this.state.manufacturer} placeholder='manufacturer' requiredtype='text' id='manufacturer' className='form-control' />
                        <label htmlFor='manufacturer'>Manufacturer</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleChangeName} value={this.state.name} placeholder='name' requiredtype='text' id='name' className='form-control' />
                        <label htmlFor='name'>name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleChangeColor} value={this.state.color} placeholder='color' requiredtype='text' id='color' className='form-control' />
                        <label htmlFor='color'>Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleChangePictureUrl} value={this.state.picture_url} placeholder='picture_url' requiredtype='text' id='picture_url' className='form-control' />
                        <label htmlFor='picture_url'>Picture of your Shoes</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={this.handleChangeBin} name='bin' className="form-select" id="bin" multiple={false}>
                            <option value="">Bin</option>
                            {this.state.bins.map(bin => {
                                return (
                                    <option key={bin.href} value={bin.href}>{bin.bin_number}</option>
                                )
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Add Shoes</button>
                </form>
            </div>
        )
    }
}

export default ShoeForm