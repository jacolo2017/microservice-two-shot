import React from "react";

class HatsForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            style_name: '',
            color: '',
            picture_url:'',
            fabric: '',
            location: '',
            locations: [],
        };


        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStyleChange = this.handleStyleChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handlePictureChange = this.handlePictureChange.bind(this);
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChangeLocation = this.handleChangeLocation.bind(this);


    }
    
    async componentDidMount() {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            
            this.setState({locations: data.locations});
        }
    }
    
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state}
        delete data.locations;
        
        
        const hatsUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
          };
          const response = await fetch(hatsUrl, fetchConfig);
          if (response.ok) {
            const newHat = await response.json();
            console.log("pls work", newHat)
            
            const cleared ={
                name: '',
                style_name: '',
                color: '',
                picture_url:'',
                fabric: '',
                location: '',
            };
            this.setState(cleared)
          }
    }

    handleNameChange(event){
        const value = event.target.value;
        this.setState({ name: value });
    }
    handleStyleChange(event){
        const value = event.target.value;
        this.setState({ style_name: value });
    }
    handleColorChange(event){
        const value = event.target.value;
        this.setState({ color: value });
    }
    handlePictureChange(event){
        const value = event.target.value;
        this.setState({ picture_url: value });
    }
    handleFabricChange(event){
        const value = event.target.value;
        this.setState({ fabric: value });
    }
    handleChangeLocation(event){
        const value = event.target.value;
        this.setState({ location: value });
    }






    render() {
        return(
            <div className="container">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Create a new hat</h1>
                            <form onSubmit={this.handleSubmit} id="create-hat-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleNameChange} required placeholder="Name"  type="text" name="name" id="name" className="form-control"/>
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange ={this.handleStyleChange} required placeholder="Style Name"  type="text" name="style_name" id="style_name" className="form-control"/>
                                <label htmlFor="starts">Style</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleColorChange} required placeholder="Color"  type="text" name="color" id="color" className="form-control"/>
                                <label htmlFor="ends">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handlePictureChange} required placeholder="Picture Url"  type="text" name="picture_url" id="picture_url" className="form-control"/>
                                <label htmlFor="ends">Picture link</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleFabricChange} required placeholder="Fabric"  type="text" name="fabric" id="fabric" className="form-control"/>
                                <label htmlFor="fabric">Fabric</label>
                            </div>
                            <div className="mb-3">
                            <select onChange={this.handleChangeLocation}  name="locations" id="locations" className="form-select">
                                <option value="">Choose a closet</option> 
                                    {this.state.locations.map(locate =>{
                                        return (
                                            <option key={locate.href} value={locate.closet_name}>{locate.closet_name}</option>
                                            )
                                        })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                    </div>
                </div>
                </div>
            );
        }
    

}
export default HatsForm;