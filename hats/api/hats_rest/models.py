from django.urls import reverse
from django.db import models

# Create your models here.

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=True)
    closet_name = models.CharField(max_length=100, null = True)
    section_number = models.PositiveSmallIntegerField(null = True, blank=True)
    shelf_number = models.PositiveSmallIntegerField(null = True, blank=True)

    def __str__(self):
        return self.closet_name

    class Meta:
        ordering = ("closet_name",)

class Hats(models.Model):
    name = models.CharField(max_length=30, null=True)
    style_name = models.CharField(max_length=60, null=True)
    color = models.CharField(max_length=20, null=True)
    picture_url = models.URLField( null=True)
    fabric = models.CharField(max_length=100, null=True)

    location = models.ForeignKey(
        LocationVO,
        related_name="location",
        on_delete= models.CASCADE,
        null=True,
    )


    def get_api_url(self):
        return reverse("api_show_hats", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name
