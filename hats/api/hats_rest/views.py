from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import Hats , LocationVO
# from wardrobe.api.wardrobe_api.views import api_locations

from common.json import ModelEncoder


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
    ]


class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = ["name"]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}


class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
    "name",
    "color",
    "fabric",
    "style_name",
    "picture_url",
    ] 
    def get_extra_data(self, o):
        return {"location": o.location.closet_name}


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hat = Hats.objects.all()
        return JsonResponse(
            {"hats": hat},
            encoder= HatsListEncoder,
        )
    else: 
        content = json.loads(request.body)
        try:
            location = LocationVO.objects.get(closet_name=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Does Not Exist"},
                status = 400,
            )

        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder= HatsDetailEncoder,
            safe=False,
        )



@require_http_methods(["DELETE", "GET"])
def api_show_hats(request, pk):
    if request.method == "GET":
        try:
            hat = Hats.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder= HatsDetailEncoder,
                safe=False,
            )
        except Hats.DoesNotExist:
            return JsonResponse(
                {"message": "Does Not Exist"},
                status=400,
            )

    else:
        try:
            count, _ = Hats.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Hats.DoesNotExist:
            return JsonResponse(
                {"message": "Does Not Exist"},
            )
